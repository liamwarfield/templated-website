---
title: "It's to Crazy of a World not to Share"
date: 2020-05-10
tags: ["themes"]
draft: false
---

# Introduction

Hey with all the craziness in the world right now I decided I should start a blog. I want this blog to be a place where I can share fun tech stuff, my analysis of world events, and slice of life things that I want to share. Prepare for blog posts on mead making, OS creating, and occasionally reflecting.

![Purple link is better than green link](https://liam.warfiel.dev/link.png)