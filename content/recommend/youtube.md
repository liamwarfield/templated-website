---
title: "Youtube"
description: "These chanels rock!"
# repo: "https://gitlab.com/liamwarfield/personal-website" # delete this line if you want a blog-like page
weight: 1
draft: false
---

# Education
## [Historia Civilis](https://www.youtube.com/channel/UCv_vLHiWVBh_FR9vbeuiY-A)
Best to see the fall of the republic! Historia Civilis is able to convey a surprisingly wide range
of emotions, actions, and tactics with just squares!
## [Tier Zoo](https://www.youtube.com/channel/UCHsRtomD4twRf5WVHHk-cMw/featured)
Learn all about the MMORPG known as outside. Tier Zoo does deep dives into both the current anthropocene meta and historical
metas. 
## [Live Overflow](https://www.youtube.com/channel/UClcE-kVhqyiHCcjYwcpfj9w)
Great channel for beginner hacking.
## [Ben Eater](https://www.youtube.com/user/destinws2)
By far the largest channel on this list, but for good reason. Destin is great at teaching and being a good person!

# Film and Storytelling
## [Filmento](https://www.youtube.com/user/Filmento)
Get ready for a meme/shitpost riddle deep dive into the success or more often failures of major movies.
## [Movies with Mikey](https://www.youtube.com/user/chainsawsuitoriginal)
Another good analysis channel, more serious than Filmento.
## [Overly Sarcastic Productions ](https://www.youtube.com/channel/UCodbH5mUeF-m_BsNueRDjcw)
A channel based off of trope talks, myths, and history. 

# Other
## [Day9](https://www.youtube.com/channel/UCaxar6TBM-94_ezoS00fLkA)
My favorite gaming youtuber. Day9 is hilarious and (mostly) good at the games he plays. I use this chanel when I just need something
going on in the background. The Day9 story time videos are great, and I would recommend participating in one of his 30 day projects!
## [Louis Rossman](https://www.youtube.com/channel/UCl2mFZoRqjw_ELax4Yisf6w)
A channel on computer repair, small business, and NYC.
## [Rare Earth](https://www.youtube.com/channel/UCtGG8ucQgEJPeUPhJZ4M4jA)
Chris Hadfield's son's youtube channel. It is a very serious channel that talks about history. I recommend starting [here](https://www.youtube.com/watch?v=MS5objr5dI0&t=). 