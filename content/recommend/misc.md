---
title: "Miscellaneous"
description: "This is were all the odds and ends that can't fill a page end up."
# repo: "https://gitlab.com/liamwarfield/personal-website" # delete this line if you want a blog-like page
weight: 1000 # I want this to always be last
draft: false
---

# [Google Technical Writing Course](https://developers.google.com/tech-writing)
This is every engineer's best friend. This will teach you how to write technical documentation
that reads well and conveys meaning to other engineers. This will make you better at what most
engineers are ABYSMALLY bad at! 10/10 would recommend again!
 
# [Rules For Rulers](https://www.youtube.com/watch?v=rStL7niR7gs)
A quick and friendly video by CGP grey on how power structures work. It's worth knowing how
the cogs and machinery work around you.
 
# [Mr. Money Mustache (MMM)](https://www.mrmoneymustache.com/)
A good blog on militant financial independence. I recommend looking into the back catalog over
the newer stuff.
 
# [Paul Christoper Slides on being a Career Software Engineer](https://docs.google.com/presentation/d/11peF7GkadekGSCjxbKYoccuH7gGVB4yZMcrm-6nxGAI/edit?usp=sharing)
Wysiwyg

# [Oh Shit Git](https://ohshitgit.com/)
OH SHIT I DID SOMETHING! HOW FIX?

# Swing Dancing
Incredibly fun, everyone should give it (or any other type of dancing) a try! You will
meet very interesting people along the way!
 
# Glass Blowing
I was lucky enough to learn glass blowing while I was studying metallurgical engineering.
Glassblowing is a lot of fun, creates good habits for working in dangerous environments,
and makes pulling things out of the oven trivial. I would only suggest it if your university
has a glass shop, or if your willing to be an apprentice -- Your wallet will melt if you try
to pay for this yourself. You WILL get burned eventually.
 
# Aloe Plant
The only plant in the aforementioned glass shop. The "leaves" contain a VERY effective anti-burn
gel (also it's nice on your skin). 10/10 best house plant.
 
# [The Other Warfield Twin](https://sam.warfiel.dev)
See his website here, do not let him convince you that green link is better than purple link.
