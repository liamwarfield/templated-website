---
title: "Hardware"
description: "Here's a nickle, go by yourself a real computer"
# repo: "https://gitlab.com/liamwarfield/personal-website" # delete this line if you want a blog-like page
weight: 1
draft: false
style:
---
<style>
  .content {
    text-align: left;
  }

  .content h1, h2, h3, h4, h5, h6, img {
    text-align: center;
    align-content: center;
    display: block;
    margin-left: auto;
    margin-right: auto;
  }
</style>

## Pixelbook Go
![https://arstechnica.com/gadgets/2019/10/pixelbook-go-review-a-cheaper-pixelbook-does-not-come-without-compromises/](https://liam.warfiel.dev/pixelbookgo.jpg)
It is:
* Small
* Durable
* Light
* **Long lasting battery**

It is not:
* Powerful

Out of all the laptops I've owned over the years, this is the one I'd want to command a nuclear war with.
First of all it never leaves me because it's so light that I don't notice it in my backpack. This makes perfect
for doing work on the train, or dealing with a Korilian Death lazer being pointed at a production database.
It's also tough a bugger! I've dropped the poor PixelBook while skiing and it didn't even care. It gives me
confort that one slip up will take me out of the game. Most importantly, I never worry about the battery life
of this little guy. I've gone 3-5 days without charging and doing moderate work. Unlike a heavy weight gaming 
laptop, I'll never have to worry about doing something quickly because I'm off grid.

## Nintendo Switch
![switch > all other consoles](https://liam.warfiel.dev/switch-logo.svg)

It is:
* **Fun**
* Light
* Portable
* Hackable

It is not:
* Powerful
* Good for poeple with large hands