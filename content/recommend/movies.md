---
title: "Movies"
description: "This is a list of movies that I have watched and would recommend for others. If you don't see
              your favorite movie here, I have either not watched it or did not particularly like it."
# repo: "https://gitlab.com/liamwarfield/personal-website" # delete this line if you want a blog-like page
weight: 1
draft: true
---
# The Big Short