---
title: "Linux Distro's"
description: "I'm not here to start a flame war, but probably will anyways"
# repo: "https://gitlab.com/liamwarfield/personal-website" # delete this line if you want a blog-like page
weight: 1
draft: false
---

# Distros
## The Beginner
These are the distros that are easy to install and get working. Support is easy
to find online.
### Ubuntu
This is baby's first distro and I'd honestly recommend everything (but Kali)
in the developer section over this one.
 
## The Developer
These distros are fantastic for the average developer, either due to feature set or
ubiquity in the industry.
### Fedora
Fedora is a distro created by RedHat as an upstream for CentOs. It is closer to the
bleeding edge than all but Arch. It is very easy to install using the Redhat’s media
writer. I use it for my daily driver.
 
Fedora uses the `dnf` package manager. `dnf` is very easy to interact with and use, I
consider it better than `apt`. The only drawback to `dnf` is that it is coded in python and
can be much slower than other package managers.  In my experience this really becomes
a problem when you have many repositories. CentOs and Fedora common production environments in industry,
so knowing Fedora can give you a leg up.
 
### Debian
This is a tried and true distro. Debian is renowned for its stability and large user base.
It's a solid choice for developers. Debian is what Ubuntu is based off of. Debian uses the `apt`
package manager, don't expect packages to be as up to date as Fedora or Arch.
 
### Kali
Two disclaimers:
1. I am not a security researcher, but I have used Kali in the past to do hack the box and network
  debugging.
2. I have not tried Cyber Security focussed distros.
 
That being said, I think that Kali is a feature rich distro that's great! It has a stupendous
amount of cybersecurity tools right out of the box, and I like to use it to track down
network issues. It comes in multiple flavors (light, heavy, and strawberry :p), and
is debian based. It uses `apt`, but beware that it does not come with any default repo
to pull from for updates or packages (so that you can specify where you want your software
to come from).
 
## The Jedi
These are the distros for the people who want to see how the sausage is made.
### Arch
Arch is a process to install, the wiki is god tier, and `pacman` is a FANTASTIC package manager.
### Gento
Compiling from source make computer go brr...