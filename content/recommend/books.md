---
title: "Books"
description: "This is a list of books that I have read and would recommend for others. If you don't see
              your favorite book here, I have either not read it or did not particularly like it."
# repo: "https://gitlab.com/liamwarfield/personal-website" # delete this line if you want a blog-like page
weight: 1
draft: false
---

## Cryptonomicon - Neil Stephenson
## The City & the City - China Miéville
## POC||GTFO