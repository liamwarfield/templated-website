---
title: "From Scratch Personal Website"
description: "A version of this website entirely made by me, its not pretty"
# repo: "https://gitlab.com/liamwarfield/personal-website" # delete this line if you want a blog-like page
tags: ["html", "css", "js"]
weight: 1
draft: false
---

Find it [here](https://gitlab.com/liamwarfield/personal-website) on gitlab.
Look at the deployed version [here](https://liamwarfield.gitlab.io/personal-website/).