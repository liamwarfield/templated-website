---
title: "This Website"
description: "What you see is what you get."
# repo: "#" # delete this line if you want a blog-like page
tags: ["html", "css", "js"]
weight: 2
draft: false
---

Find it [here](https://gitlab.com/liamwarfield/templated-website) on gitlab.
Look at the deployed version [here](https://liam.warfiel.dev/).